#!/bin/bash

if [ "$STAGING" = true ]; then
  echo "Using Let's Encrypt Staging environment..."
  certbot -n --staging --expand --apache --agree-tos --email $WEBMASTER_MAIL --domains $DOMAIN
else
  echo "Using Let's Encrypt Production environment, listing certificates..."
  certbot certificates
  if [[ -f /etc/letsencrypt/live/$DOMAIN/fullchain.pem && -f /etc/letsencrypt/live/$DOMAIN/privkey.pem ]]; then
     echo "Certificates exist, skiping their generation"
     if [[ -f /etc/apache2/sites-enabled/000-default-le-ssl.conf ]]; then
         echo "Link to ssl conf exists, skiping link generation"
     else
         ln -s /etc/apache2/sites-available/000-default-le-ssl.conf /etc/apache2/sites-enabled/000-default-le-ssl.conf
     fi
  else
     echo "Certificates not found, generating them..."
     certbot -n --expand --apache --agree-tos --email $WEBMASTER_MAIL --domains $DOMAIN
     chmod -R 755 /etc/letsencrypt
     ln -s /etc/apache2/sites-available/000-default-le-ssl.conf /etc/apache2/sites-enabled/000-default-le-ssl.conf
  fi
fi

