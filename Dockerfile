FROM phusion/baseimage:0.10.1

ENV DEBIAN_FRONTEND noninteractive

CMD ["/sbin/my_init"]

# MAIN DEPENDENCIES
RUN apt-get -y update && \
    apt-get install -q -y curl apache2 nano software-properties-common && \
    add-apt-repository ppa:certbot/certbot && \
    apt-get -y update && \
    apt-get install -q -y python-certbot-apache && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# APACHE CONFIGURATION
ADD config/mods-available/proxy_html.conf /etc/apache2/mods-available/
ADD config/conf-available/security.conf /etc/apache2/conf-available/
ADD config/sites-available/* /etc/apache2/sites-available/

RUN echo "ServerName localhost" >> /etc/apache2/conf-enabled/hostname.conf && \
    a2enmod ssl headers proxy proxy_http proxy_html xml2enc rewrite usertrack remoteip && \
    a2ensite 000-default && \
    mkdir -p /var/lock/apache2 && \
    mkdir -p /var/run/apache2

# configure runit
RUN mkdir -p /etc/service/apache
ADD config/scripts/run_apache.sh /etc/service/apache/run
ADD config/scripts/generate_certs.sh /etc/my_init.d/

EXPOSE 80
EXPOSE 443
